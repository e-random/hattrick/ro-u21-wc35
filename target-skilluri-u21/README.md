# Skill-uri U-21

**ACEASTA VERSIUNE ESTE UN DRAFT PUS IN DISCUTIE PE CONFERINTA NATIONALA**

Exista doua abordari disponibile pentru construirea unui lot de U21:

1. Alegerea jucatorilor de top din generatie si modelarea unei echipe in functie de acestia, cu focus pe avantajele acestui grup de jucatori.
2. Crearea a nenumarate tipuri de jucatori, pentru toate gusturile.

Romania fiind o tara cu un pool de antrenori si jucatori foarte mare, isi permite sa incerce ambele abordari. 

## Abordarea 1: jucatorii de top

Proiectele de monitorizare se concentreaza pe abordarea 1: jucatorii de top din fiecare generatie sunt recomandati managerilor care ofera conditii financiare si de antrenament de top si o colaborare cat mai buna in vederea dezvoltarii pe ansamblu a lotului. In aceasta categorie intra manageri care sunt dispusi sa sacrifice cate ceva atat din punct de vedere financiar (pentru ca jucatorii de top sunt _foarte scumpi_) cat si din punct de vedere al flexibilitatii in antrenament (pentru ca jucatorul sa primeasca antrenament pe skill-urile relevante la momentele potrivite pentru U21).

In aceasta abordare, nu pot fi publicate in avans tinte de skill-uri pentru ca ele depind in mare masura de profilele si specialitatile pe care ies jucatorii de top. Cateva lucruri totusi sunt stiute:

* Este nevoie de antrenor excelent.
* Este nevoie de medic de nivel 5.
* Este nevoie de 2 asistenti de nivel total 10.
    * Ideal, acestia sunt angajati cu contracte de o saptamana joia, inainte de antrenament, din doua in doua saptamani. Asta ajuta la scaderea riscului de accidentari, iar costul in plus este neglijabil. Dar asta necesita foarte multa atentie! 
* Este nevoie de antrenor de forma in aproprierea unei potentiale selectii.
* Este nevoie de flexibilitate in antrenament pe segmentul 20-22 de ani.
* Este nevoie ca jucatorii sa joace pe pozitie antrenabila in meciul de sambata.
    * In cazul unei accidentari, astfel se asigura cat mai mult timp de recuperare.
* Este nevoie ca jucatorii sa joace cat mai multe meciuri oficiale.
    * Asta inseamna ca atat timp cat echipa este in orice fel de cupa, jucatorii trebuie sa joace meciul de cupa pe pozitie neantrenabila.

## Abordarea 2: Varietate in profilurile de jucatori disponibile.

Abordarea 1 functioneaza doar in masura in care indeajuns de multi manageri aleg sa se implice activ in formarea unei generatii. Dar ea nu poate fi _singura_ abordare.

Comunitatea, prin multii jucatori promovati si multii manageri care sunt interesati de a ii antrena cat mai aproape de optim cu putinta, se concentreaza natural pe abordarea 2: in functie de preferintele fiecarui manager si de jucatorii disponibili sunt create nenumarate tipuri de jucatori, pentru toate gusturile. Aici investitiile in jucatori sunt mai mici, iar planul de antrenament mai stabil. Desi bineinteles, toate elementele de la abordarea 1 constituie avantaje.

Pentru aceasta abordare se pot publica in avans atat principii de constructie ale fiecarei pozitii cat si skill-uri de jucatori care pot avea sanse destul de serioase la selectie.

### Portari

O data cu trecerea la U21 devine eficienta formarea portarilor din fundasi imprevizibili. Planul de antrenament pentru acest tip de jucator e relativ simplu:

1. E necesar un fundas cu Aparare 6-7, Pase 5-6, imprevizibil
2. Se antreneaza portar pana in momentul in care se creste stamina.
3. Se antreneaza SP pana la 15 pe stamina crescuta.
4. Se trece inapoi pe portar.

Pe segmentul de portari clasici, antrenamentul este identic cu exceptia faptului ca va fi nevoie de putin antrenament de aparare (deoarece portarii clasici nu pot plecat cu mai mult de 5 aparare).

1. Se antreneaza portar pana in momentul in care se creste stamina.
2. Se antreneaza SP pana la 15 pe stamina crescuta.
3. Se alterneaza portar si defending in functie de care antrenament aduce mai mult aport rating-urilor.
    * Nota: Nu este o tragedie daca alternarea antrenamentului nu este posibila si se ramane pe portar pana la finalul campaniei. E optim sa se alterneze, dar castigul este foarte mic.

### Fundasi

**Pentru WB** (fundasii laterali), lucrurile sunt simple. Ei au nevoie in primul rand de extrema - daca skill-ul de extrema nu aduce un aport semnificativ la atacul lateral atunci poate fi preferat un SP Taker sau un WBTM pe acea pozitie. Deci planul de antrenament arata cam asa:

1. Extrema pana la 12-13. 
2. Aparare pana la capat.

**Pentru CD** (fundasii centrali), abordarea este foarte similara doar ca aici vorbim de importanta skill-ului de constructie. In cazul in care aportul la mijloc nu este semnificativ, aici poate fi preferat SP Taker-ul sau un CDO. Deci planul arata cam asa:

1. Constructie pana la 10-11.
2. Aparare pana la capat.

**Pentru CDO** (fundasii centrali ofensivi), aceasi abordare este valabila ca pentru CD, dar in sens invers. Aici skill-ul care trebuie sa prinda un nivel minim pentru a nu face jucatorul nerelevant este apararea. Plan de antrenament:

1. Aparare pana la 11-12
2. Constructie pana la capat.

**Pentru CD/WB-CA** (fundasii "de CA"), lucrurile devin usor complicate pentru ca sunt mai multe tipuri de CA. Daca discutam de un CA extrem, atunci constructia nu este relevanta si sunt doriti fie jucatori care sunt antrenati exclusiv la pase si la aparare, fie jucatori care sunt antrenati mult la aparare dar primesc si putin pase sau extrema. Daca in schimb discutam de un CA cu posesie, atunci extrema devine irelevanta si ar trebui inlocuita cu constructie pentru a aduce dramul de posesie care face diferenta. Un plan de antrenament flexibil ar fi:

1. Aparare pana la 14.
2. Pase pana la 8.
3. Contactat monitorii sau antrenorul de la momentul respectiv pentru a stabili care din profilele de mai sus se potriveste cel mai bine pentru imaginea de ansamblu a generatiei. Se poate merge pe pase spre 9-10, extrema spre 8-9 sau constructie spre 8-9, in functie de secundarele jucatorului si de ce lipseste din peisajul lotului extins.


**Pentru CD/WB-SP** (fundasii executanti de lovituri libere), datorita faptului ca portarii vor ajunge la 17, e necesar ca skill-ul de SP sa fie minim 17 la 21 de ani si ca jucatorul sa fie in continuare antrenat la SP. De aceea planul de antrenament este foarte simplu:

1. Defending pana la 14.
2. SP.

### Mijlocasi

**Pentru IMN/WTM** (mijlocasi normali, fara ordin individual - acestia sunt jucati foarte des si ca extreme cu ordin catre mijloc), se antreneaza numai constructie. O exceptie poate fi generata de specialitatea jucatorului. In functie de asta, pentru a beneficia la maxim de abilitati (sau pentru a n-o tranforma in dezavantaj), e posibil sa mai fie nevoie de ceva pase sau atac. Deci planul de antrenament ar arata astfel.

1. Constructie.
2. Posibil ajustare secundare in functie de specialitate:
    * U: Ideal minim 6 pase, minim 5 scoring
    * Q: Ideal minim 6 pase, minim 5 scoring.
    * H: Ideal minim 5 scoring.
    * T: Ideal minim 5 scoring.
    * \*: Ideal minim 5 winger.
    * Nota: punctele acestea sunt listate in ordinea prioritatilor, dar nici unul din ele nu este esential. Poate da un bonus jucatorului in fata altor jucatori identici, dar esential este skill-ul principal. Iar aceste ajustari se pot face si dupa varsta de 21 de ani, cand deja devine clar daca jucatorul este interesant in generatia lui, pentru ca aceste nivele vor fi atinse foarte repede.

**Pentru DIM** (mijlocasi cu ordin individual defensiv), e nevoie de un nivel de defending indeajuns de mare pentru a putea a genera un nivel de contributie satisfacator la rating-urile de aparare, dar fara a lua foarte mult din rating-ul de la mijloc. Cei puternici pot si presa ocazii, dar cum majoritatea jucatorilor de U21 au scoring nativ sau ridicat la minim, un nivel de 8-9 defending este suficient, urmand ca restul antrenamentului sa mearga in constructie. Plan:

1. Aparare pana la 8-9.
2. Constructie pana la capat.

**Pentru IMO/DF** (mijlocasi cu ordin individual ofensiv/atacanti cu ordin individual defensiv), trebuie tinut cont de faptul ca trebuie balansata contributia lor la mijloc cu contributia lor in atac. Jucatorii care pot juca una dintre pozitii o pot juca in general satisfacator si pe cealalta, de aceea sunt tratati impreuna, dar optim un atacant defensiv ar avea ceva mai mult atac si pase (in dauna constructiei) fata de un mijlocas ofensiv. Planul de antrenament pentru un jucator care este clar IMO arata cam asa:

1. Constructie pana la 14.
2. Atac pana la 7.
3. Pase pana la capat.

**Pentru IMTW/WTM** (mijlocasi cu ordin indiviual catre extrema/extreme cu ordin individual catre mijloc), se aplica in mare aceleasi considerente ca la IMO/DF, doar ca aici skill-ul principal poate fi considerat extrema in loc de pase si constructia este o secundara mai importanta decat la IMO/DF. Astfel, un plan de antrenament ar fi:

1. Constructie pana la 15.
2. Extrema pana la capat.

### Extreme

**Pentru WN** (extrema normala), trebuie sa asiguram in primul rand contributia la atacul lateral, iar apoi contributia la mijloc. Din pacate nu prea mai ramane loc pentru optimizare de secundare necesare pentru specialitati, deci aici se prefera jucatori care sunt promovati cu secundare necesare pentru specialitatile lor deja la nivel 5-6. Planul de antrenament e clar:

1. Extrema pana la 15.
2. Playmaking pana la capat.

**Pentru WO** (extrema cu ordin individual ofensiv), suntem interesati exclusiv de contributia la atacul lateral. Asemenea jucatori se folosesc in general fie intr-un CA extrem, fie impotriva unui CA. Din cauza variatiei foarte mare intre nivelurile secundarelor totusi, este posibil ajustarea acestora pe final. Planul ar arata cam asa:

1. Extrema pana la 17.
2. Pase pana la 9.
3. Ajustare secundare (constructie, aparare) sau continuare antrenament pe extrema/pase in functie de cum arata jucatorul si de feedback-ul monitorilor sau al antrenorului de la momentul respectiv.

### Atacanti

**Pentru DF/IMO** (atacanti cu ordin individual defensiv/mijlocasi cu ordin individual ofensiv), trebuie tinut cont de faptul ca trebuie balansata contributia lor la mijloc cu contributia lor in atac. Jucatorii care pot juca una dintre pozitii o pot juca in general satisfacator si pe cealalta, de aceea sunt tratati impreuna, dar optim un atacant defensiv ar avea ceva mai mult atac si pase (in dauna constructiei) fata de un mijlocas ofensiv. Planul de antrenament pentru un jucator care este clar DF arata cam asa:

1. Constructie pana la 11.
2. Atac pana la 8.
3. Pase pana la capat.


**Pentru FWN** (atacanti jucati fara ordin individual), trebuie sa tinem cont de faptul ca sunt singura pozitie in afara de IMO care contribuie semnificativ la rating-ul atacului central, dar ca in acelasi timp contribuie si la rating-ul mijlocului. Exista aici un compromis care trebuie facut intre primirea si conversia ocaziilor. Planul de antrenament propus este:

1. Constructie pana la 8. In princiu.
    * Dar ar putea fi pana la 10 daca specialitatea jucatorului este Puternic, pentru ca ajuta foarte mult la event-urile specifice.
    * Sau ar putea fi sa nu fie antrenat deloc daca specialitatea jucatorului este Rapid sau Imprevizibil pentru ca atat pasele cat si scoring-ul ajuta foarte mult la specialitatile lor.
2. Pase pana la 8. In principiu.
    * Sau 9 daca nu s-a antrenat constructie.
    * Sau 6 daca s-a mers pe constructie 10.
3. Atac pana la capat.

### Executanti de penalty-uri

O categorie speciala de jucatori care nu este prevazuta in formarea altor nationale este formata din jucatorii cu doar indeajuns de mult SP incat sa fie eficienti in executarea penalty-urilor si a tranformarea event-urilor de corner + cap. I-am notat in tabelul de mai jos **\*-SP**.

In unele meciuri, scaderea de rating cauzata de folosirea unui WB/CD SP este considerata de selectioner prea mare, si poate deveni folositor sa existe un alt jucator, care are SP undeva in jur de 11 dar altfel este antrenat pe pozitia lui normala.

Acesti jucatori pot fi formati de pe orice pozitie, urmand a fi antrenati la SP dupa varsta de 21 de ani. Din acest motiv, singurul skill mentionat in tabelul de mai jos este 5 ca skill nativ pentru SP.


### Sumar

In concluzie, intr-o forma scurta, intr-un tabel, ar arata ca mai jos. Tineti cont ca aceste numere sunt calculate:

* cu antrenor solid
* plecand de la skill 7 pentru principal
* plecand de la skill 6 pentru secundara (mai putin portar, 5 aparare respectiv, pentru imprevizibil, 1 portar)
* cu stamina 10% (in realitate ar putea fi 15% incepand de pe la 20 de ani)
* considerand un nivel decent (5+) al celorlalte skill-uri relevante pentru pozitie
* la varsta de aproximativ 21 de ani (+ denota antrenamentul intre 21 si 22 de ani). Poate 1-2 saptamani in plus sau in minus, pentru rotunjire. Cu exceptii
    * CD/WB CA sunt calculati la 20 de ani si 56 de zile (note mai sus de ce)
    * CD/WB CA, si WO nu primesc (+) deoarece antrenamentul optim depinde mult de cum arata jucatorul si generatia.

Practic, atingerea acestor numere nu garanteaza in nici un fel selectia, la fel cum avand in vedere jucatori putin sub aceste numere nu garanteaza ca nu vor fi selectionati. Daca un jucator este sau nu selectat va depinde foarte mult de contextul de la momentul chemarii jucatorilor in lot. Aceste numere sunt pur orientative.

Specialitatile sunt listate in ordinea eficientei pe pozitie. Separate de `/` sunt cam la fel de eficiente, separate de `,` sunt ceva mai putin eficiente. Dar asta nu inseamna ca daca jucatorul nu are specialitatea perfecta, sau nu are specialitate deloc atunci va fi ignorat.


| Position | Specs         | GK   | DE     | PM     | WG     | PA      | SC       | SP      |
|----------|---------------|------|--------|--------|--------|---------|----------|---------|
| GK       |               | 17+  | 5+     |        |        |         |          | 15      |
| GK       |  U            | 16+  | 7      |        |        | 6       |          | 15      |
| WB       |  Q/U/H,T      |      | 13+    |        | 12/13  |         |          |         |
| CD       |  U/H/Q/T      |      | 13+    | 10/11  |        |         |          |         |
| CDO      |  P/H, Q/U     |      | 10/11  | 13.5+  |        |         |          |         |
| CD/WB CA |  U/T, H/Q     |      | 14     |        |        | 8       |          |         |
| CD/WB SP |  Q/U, H/T     |      | 14     |        |        |         |          | 17+      |
| DIM      |  P, Q/H       |      | 8-9    | 15+    |        |         |          | 15      |
| IM       |  U/H/Q/T      |      |        | 16+    | 5?     | 6?(U/Q) | 5?(U/Q/H/T) |         |
| IMO      |  U/Q, T       |      |        | 14     |        | 10+     | 7        |         |
| IMTW/WTM |  Q/U/H/T      |      |        | 15     |  10+   |         |          |         |
| WN       |  Q/U/H/T      |      |        | 11/12+ |  15    |         |          |         |
| WO       |  Q/U          |      |        |        |  17    | 9       |          |         |
| DF       |  T,U,Q        |      |        | 11     |        | 13+     | 8        |         |
| PNF      |  P            |      |        | 10     |        | 6       | 14+      |         |
| NF1      |  H/U/Q, T/P   |      |        | 8      |        | 8       | 14.5+    |         |
| NF2      |  U/Q, H/T     |      |        |        |        | 9       | 15+      |         |
| *-SP     |  *            |      | *      | *      | *      | *       | *        | 5+      |
