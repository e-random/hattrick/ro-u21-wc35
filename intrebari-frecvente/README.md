# Intrebari frecvente

Aceste intrebari au fost puse frecvent, fie pe Discord fie pe topicul dedicat din HT.

## Monitoriazare

### 1. Cu cine lucrez?

Raspunsul e in [lista de colaboratori de pe prima pagina](../). Daca credeti ca nu e updatata, deschideti un issue!

### 2. Cum lucrez?

Raspunsul e in [ghidul monitorului](../ghidul-monitorului). Daca aveti intrebari punctuale, deschideti un issue! (nu va multumiti cu o explicatie in privat pe HT-YC)

### 3. De ce nu doar un fisier pe google drive? De ce nu doar tracker-ul intern? De ce nu doar tracker-ul international de seniori? De ce nu doar tracker-ul international de juniori?  De ce nu pot sa contactez un antrenor inainte de a adauga jucatorul in GitLab? De ce nu doar discord? De ce nu doar HT-Mail? De ce nu doar conferinta nationala? De ce e asa complicat?

Daca vei lua lucrurile pas cu pas, vor deveni mai putin complicate. Problema este ca avem o baza foarte mare de jucatori, suntem multi oameni, si e foarte usor sa ne calcam unii pe altii pe piciaore. Avem nevoie de o modalitate de a ne organiza task-urile si de a centraliza toate informatiile adunate, si nici unul din tool-urile de mai sus nu indeplineste toate cerintele.

GitLab nu este perfect, si poate candva com gasi o alternativa mai buna. Dar pana atunci este singurul din tool-urile de mai sus care are si un issue tracker, ne ofera si notificari, este si public si gratis, si ofera si suport pentru ghiduri si publicare de informatie transparent. Multi din cei care au acumulat putina experienta in a lucra cu el considera ca per total chiar usureaza munca in loc sa fie "inca un loc" de care trebuie sa tinem cont.
