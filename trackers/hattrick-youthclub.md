# Hattrick Youthclub

Program CHPP. [Detalii licenta si suport/Licence and support details](https://www.hattrick.org/goto.ashx?path=/Community/CHPP/ChppProgramDetails.aspx?ApplicationId=12)


## Inscriere

1. Intai intra pe https://www.hattrick-youthclub.org/site/register si creaza un cont.
2. La prima logare, jucatorii de tineret de la echipa ta principala vor fi adaugati automat.
3. Pentru a avea si celelalte echipe scanate, trebuie sa schimbi in mod explicit echipa din partea de sus a ecranului.
4. Sfatuim folosirea acestui site in mod constant (intrare dupa fiecare meci de juniori) deoarece contine foarte multe elemente foarte folositoare pentru academie.
