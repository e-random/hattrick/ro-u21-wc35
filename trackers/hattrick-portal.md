# Hattrick Portal Tracker

Program CHPP. [Detalii licenta si suport/Licence and support details](https://www.hattrick.org/goto.ashx?path=/Community/CHPP/ChppProgramDetails.aspx?ApplicationId=4678)

## Inscriere

### Romanian

1. Intra pe http://www.hattrickportal.pro/tracker si introdu informatiile necesare pentru a autoriza site-ul.
2. Odata ce te-ai logat, du-te in pagina care se numeste "Tracker" si uita-te in partea din dreapta sus a paginii (chiar sub butonul de "login/logout"). Acolo este un dreptunghi cu titlul "Tracking your players". Ai grija ca in el sa fie marcata optiunea "Yes".
3. Din acest moment site-ul va scana automat jucatorii tai.

### English

1. Go to http://www.hattrickportal.pro/tracker and enter your information to grant permission to the tracker.
2. Once logged in, go to the page titled "Tracker" and look in the upper right corner of the page below the "login/logout" button. There is a box titled "Tracking your players". Make sure the box is checked "yes".
3. From this points onwards, the website will update your players automatically.
