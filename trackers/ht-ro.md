# HT-RO

Program CHPP. [Detalii licenta si suport/Licence and support details](https://www.hattrick.org/goto.ashx?path=/Community/CHPP/ChppProgramDetails.aspx?ApplicationId=4770)

## Inscriere

### Romana

1. Intai intra pe https://www.ht-ro.org/registration/register.html si creaza un cont.
2. Dupa ce te loghezi, intra pe https://www.ht-ro.org/site/new-oauth.html si autorizeaza site-ul sa iti citeasca jucatorii.
3. Dupa ce ai facut autorizarea, intra pe https://www.ht-ro.org/site/scan.html pentru prima scanare a jucatorilor echipei tale (urmatoarele scanari vor fi efectuate automat)

### English

1. First go to https://www.ht-ro.org/registration/register.html and create an account.
2. Once you logged in, go to https://www.ht-ro.org/site/new-oauth.html and authorize the website to scan your players.
3. Once you authorized, go to https://www.ht-ro.org/site/scan.html for the first scan of your players (further scans will be performed automatically).
