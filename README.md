# WC35

Acest proiect se ocupa de monitorizarea generatiei de jucatori romani eligibila pentru a juca Cupa Mondiala 35.

## Confidentialitate si securitate

Mai jos gasiti lista celor care lucram la acest proiect, cu toate datele noastre de contact. In momentul in care cineva va contacteaza, asigurati-va ca este in lista de mai jos.

Fiecare jucator pe care il urmarim are un issue creat in sectiunea [Issues](https://gitlab.com/e-random/hattrick/ro-u21-wc35/-/issues) a proiectului. **Daca doriti ca notele jucatorului vostru sa _nu_ fie publice, va rugam sa cereti marcarea issue-ul lui ca si confidential.**. In felul asta, notele vor fi disponibile strict celor din lista de mai jos si selectionerului, in momentul in care acesta isi va prelua postul.

## Staff

| Rol Proiect         | [HT](https://www.hattrick.org) Username | [HT-RO](https://www.ht-ro.org/) Username| Gitlab Username | Discord (Username/UserID) | [HT-YC](https://www.hattrick-youthclub.org/site/nt_scouts/country/36) Username |
|---------------------|----------------------------------------------------------------------------------------|-----------------------|----------------|---|---|
| Organizator         | [KingRandom](https://www.hattrick.org/goto.ashx?path=/Club/Manager/?userId=13618995)   | KingRandom | @KingRandom | [KingRandom/andrei-e-random#0723](https://discordapp.com/users/727041306584678460/) | [KingRandom](https://www.hattrick-youthclub.org/site/user/id/351299) |
| Coordonator GK      | [werty](https://www.hattrick.org/goto.ashx?path=/Club/Manager/?userId=5162005)         | werty | @voicu.florin.eugen | [werty/werty#2887](https://discordapp.com/users/415652701460758529/) | [werty](https://www.hattrick-youthclub.org/site/user/id/364657) |
| Coordonator CD + WB | [parazytu17](https://www.hattrick.org/goto.ashx?path=/Club/Manager/?userId=9894469)    | parazytu17 | @parazytu17 | [Parazytu17/Parazytu17#6200](https://discordapp.com/users/406509119219171328/) | [parazytu17](https://www.hattrick-youthclub.org/site/user/id/368875) |
| Coordonator IM      | [werty](https://www.hattrick.org/goto.ashx?path=/Club/Manager/?userId=5162005)         | werty | @voicu.florin.eugen | [werty/werty#2887](https://discordapp.com/users/415652701460758529/) | [werty](https://www.hattrick-youthclub.org/site/user/id/364657) |
| Coordonator FW      | [Ferrago](https://www.hattrick.org/goto.ashx?path=/Club/Manager/?userId=2052778)       | Vornic | @Ferrago | [Ferrago/Ferrago#8900](https://discordapp.com/users/808038248638316595/) | [Ferrago](https://www.hattrick-youthclub.org/site/user/id/369507) |
| Monitor CD/WB       | [luca_toni_22](https://www.hattrick.org/goto.ashx?path=/Club/Manager/?userId=5545949)  | luca_toni_22 | @luca_toni_22 | [luca_toni_22/luca_toni_22#7384](https://discordapp.com/users/802242561891762266/) | [luca_toni_22](https://www.hattrick-youthclub.org/site/user/id/353491) |
| Monitor IM          | [Brunosul](https://www.hattrick.org/goto.ashx?path=/Club/Manager/?userId=12268829)     | Tudor | @Brunosul | [Brunosul/flashback#7901](https://discordapp.com/users/693885964376080455/) | [Brunosul](https://www.hattrick-youthclub.org/site/user/id/368902) |
| Monitor WG          | [cristi_cr3us](https://www.hattrick.org/goto.ashx?path=/Club/Manager/?userId=13327080) | cristi_cr3us | @cr3us | [cr3us/cr3us#9869](https://discordapp.com/users/779983051840290848/) | [cristi_cr3us](https://www.hattrick-youthclub.org/site/user/id/357073) |
| Monitor WG          | [20_Alex_20](https://www.hattrick.org/goto.ashx?path=/Club/Manager/?userId=8268530) | 20_Alex_20 | @20_Alex_20 | [20_Alex_20/20_Alex_20#3914](https://discordapp.com/users/808767880316452864/) | [20_Alex_20](https://www.hattrick-youthclub.org/site/user/id/359302) |
| Monitor FW          | [Pav3l](https://www.hattrick.org/goto.ashx?path=/Club/Manager/?userId=13661471)    | Yibi  | @pav3ll | [Pav3l/Pav3l#5246](httpd://discordapp.com/users/809098202980155422)  | [Pav3l](https://www.hattrick-youthclub.org/site/user/id/361378) |

Verificare access Gitlab: [Lista Membri](../../project_members)


## Informatii utile

| Informatii utile | Link | Observatii |
|---|---|---|
| Eligibiliate Jucatori | [HTPortal](https://hattrickportal.pro/Tracker/U20/U21WC.aspx?WorldCup=35) | Varstele eligibile se modifica zilnic, pe masura ce jucatorii imbatranesc. Acest link arata ce varsta trebuie sa aibe astazi jucatorii eligibili |
| Skill-uri target 21 de ani | [Target Skilluri U21](./target-skilluri-u21) | Skill-urile sunt orientative, daca aveti un jucator pe care il considerati interesant luati legatura cu monitorii. |
| Discord (public) | https://discord.gg/sYapbuRyR2 | Serverul de discord e public, asigurati-va ca nu postati informatii private acolo |
| Topic conferinta nationala | [17394053.1](https://www.hattrick.org/goto.ashx?path=/Forum/Read.aspx?n=1%26nm=220%26t=17394053&v=0) | Este posibil sa nu fie de actualitate, topicurile pe conferinta se inchid automat dupa 1000 de posturi si e posibil ca acesta sa fi atins de curand limita |
| Ghidul monitorului | [ghidul-monitorului](./ghidul-monitorului) | Inca in lucru, tineti un ochi pe el pentru update-uri |
| Ghidul academiei | [ghidul-academiei](./ghidul-academiei) | Inca in lucru, tineti un ochi pe el pentru update-uri |
| Intrebari frecvente | [intrebari-frecvente](./intrebari-frecvente) | Inca in lucru, tineti un ochi pe ele pentru update-uri |
