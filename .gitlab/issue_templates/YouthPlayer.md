# Eligibilitate

Dedus din [HT-Portal](https://hattrickportal.pro/Tracker/U20/U21WC.aspx?WorldCup=35)

~"Eligibil-CE-R1-X"

# Contact si monitorizare

Nume si ID in titlu.

Comunicarea cu owner-ul in comment-uri.

| Name | Value |
| ------ | ------ |
| Youth Team Name and ID| [NNN](http://www.hattrick.org/goto.ashx?path=/Club/Youth/?YouthTeamID=XXX) |
| Link HT-Ro | [Link](https://www.ht-ro.org/site/u17_player.html?pid=XXX) |
| Link HT | [Link](https://www.hattrick.org/goto.ashx?path=/Club/Players/YouthPlayer.aspx?YouthPlayerID=XXX) |
| Link HattrickYouthClub | [Link](https://www.hattrick-youthclub.org/site/player_details/player_id/XXX) |
| Owner | [Send HT-Mail](https://www.hattrick.org/goto.ashx?path=/MyHattrick/Inbox/?actionType=newMail%26userId=XXX) |

# Specialitate
Spec: None

# Skill Actual
<details>
  <summary>Skilluri jucator - Daca jucatorul este in ht-ro/hyc sectiunea poate fi ignorata.</summary>
  
Overall: ?

| | Actual | Max | Top 3 |
|---|---|---|---|
| KG | ? | ? | ? | 
| DE | ? | ? | ? | 
| PM | ? | ? | ? | 
| WG | ? | ? | ? | 
| PA | ? | ? | ? | 
| SC | ? | ? | ? | 
| SP | ? | ? | ? | 
</details>

# Training Plan

?

