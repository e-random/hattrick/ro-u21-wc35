# Ghidul academiei

_versiunea scurta_

## Reguli de baza

1. Ma inregistrez pe https://ht-ro.org
    * Pentru instructiuni, click [aici](../trackers/ht-ro.md)
2. Ma inregistrez pe https://hattrick-youthclub.org
    * Pentru instructiuni, click [aici](../trackers/hattrick-youthclub.md)
3. Joc numai in ligi de 4 echipe.
4. Ies din liga imediat dupa ultimul meci si ma alatur alteia care incepe in 2 zile.
5. Imi setez amical imediat ce e disponihil.
6. Am 3 scouti. Toti cauta "orice tip de jucator".
7. Ii sun in ziua anterioara primului meci din saptamana.
8. Accept orice jucator de 15 ani care mi se ofera (refuz per ansamblu 4 daca e jucator de camp sau per ansamblu 3 daca e portar).
9. Imi cumpar un jucator eficient pentru a il transforma in antrenor excelent cand voi avea nevoie (batran, experienta peste 15,lider 5,cost 2-6 milioane)
10. Nu refuz niciodata ultimul jucator care mi se ofera. Oricat de prost ar arata, e ultimul. Poate e ceva de capul lui. Iar daca nu e macar nu ajunge la altcineva.

## Raportul scout-ului

Contine:

1. Un skill actual.(tot timpul)
2. Un skill potential.(tot timpul, poate fi acelasi cu skill-ul actual)
3. Un calificativ per ansamblu(uneori)
4. Specialitatea. (uneori)

Observatii:

1. Skill-urile mentionate fac parte din top 3 skill-uri potentiale. Exemple:
    * Imi spune actual 5 extrema si maxim 4 constructie. Rezulta:
        * Dupa ce ii voi afla potentialul la extrema (sa zicem ca aflu 5), un singur skill mai poate fi cu potential mai mare de 5. Deci daca gasesc aparare maxim 6, nu mai are rost sa caut atac si pase pentru ca nu vor fi mai sus de 4.
    * Imi spune actual 4 aparare, maxim 4 aparare. Rezulta:
        * E posibil sa aibe doua skill-uri cu potential mai mare de 4.
        * Din informatiile pe care le am acum, jucatorul poate foarte bine sa fie maxim 8 extrema, 8 constructie, n-am de unde sa stiu inca.
        * In acelasi timp, e si posibil sa nu aibe nici un skill cu potential mai mare decat 4.
2. Calificativul per annsamblu are o formula destul de complicata care depinde de valorile actuale si potentiale a top 3 skill-uri neantrenate (nu obligatoriu aceleasi cu top 3 potebtiale). Exemple.
    * Imi spune 6 per ansamblu.
        * Asta poate sa insemne aproape orice, de la curent si maxim 5.5 la 3 skill-uri (aproape complet antrenat, dar praf), pana la curent 4 si maxim 8 la 3 skill-uri (triplu excelent potential care pleaca din skill-uri initiale joase)
        * De notat pentru fiecare skill informatii si reluat in considerare ce inseamna per ansamblu dupa ce am mai multe informatii.
3. Specialitatea este precizata in numai 10% din rapoarte, desi 50% din jucatorii oferiti au de fapt specialitate. Prin urmare:
    * Doar pentru ca nu mi-a spus scout-ul ca nu are specialitate nu inseamna ca nu are. E posibil sa nu-mi spuna nimeni de ea si sa nu vad event-uri si s-o vad abia la promovare.

## Raportul antrenorului

Contine:

1. Skill-ul actual al unui jucator care a primit antrenament principal (daca exista minim un jucator cu skill-ul actual nexunoscut pe pozitie abtrenabila).
2. Skill-ul maxim al unui jucator care a primit antrenament secundar (daca exista minim un jucator cu skill-ul maxim necunoscut pe pozitie antrenabila).
3. Un skill maxim random al unui jucaror din echipa.(foarte des,dar nu intotdeauna)
4. Varii observatii ale caror interpretari sunt disponibile [aici](https://www.hattrick-youthclub.org/site/coachcomments_show)

De retinut:

* Un skill galben nu mai poate fi antrenat. Se poate ingalbeni chiar daca antrenorul nu m-a anuntat. Ma uit la jucatori dupa fiecare meci sa vad ce a ratat antrenorul sa-mi spuna.
* Un skill verde inca mai poate fi antrenat chiar daca actual are aceasi valoare ca potential. Imi notez cat antrenament dau in el la juniori ca sa stiu exact cat de repede o sa creasca la seniori. Daca e jucator bun, continui sa antrenez pana se ingalbeneste.
* Viteza de antrenamrnt la juniori este mai mica decat la seniori. Consult https://ht-mt.org/youth-player/skill-progress ca sa stiu cate meciuri mai are un jucator in echipa de juniori si sa-mi planific antrenamentul.
* Verific planul de antrenament fie cu monitorul(daca e prospect pentru NT), fie pe forum.
* Consider un jucator prospect pentru NT daca are suma celor mai bune 3 skill-uri minim 18 si reusesc sa le ingalbenesc.

## Ordine de meci

* Am grija sa am tot timpul rezerve. Daca cumva mi se accidenteaza portarul nu vreau sa fie trecut portar fix jucatorul de camp pe care il antrenam.
* Am grija ca jucatorul despre care vreau sa aflu skill maxim sau curent sa joace minim 44 de minute in pozitie eligibila, cu antrenament eligibil, si sa fie singurul la care poate alege antrenorul sa imi spuna.
* Joc tot timpul creativ (cu cat mai multe event-uri cu atat mai mare sansa sa am un event la un jucator de care nu stiam inca daca are specialitate).
* Joc tot timpul atancatii a caror specialitate nu o stiu "normal" (poate imi apare in meci event de PNF).
* Joc tot timpul mijlocasii a caror specialitate nu o stiu "defensiv" (poate imi apare in meci event de PDIM).
* Ma uit tot timpul la raportul meciului sa caut event-uri ale unor jucatori despre care poate nu stiu daca au specialitate.
* Setez tot timpul executant de lovituri libere un jucator care are deja specialitate cunoscuta, preferabil Cap (pentru ca event-ul de corner + cap sa aibe de ales din cat mai multi jucatori a caror specialitate nu o stiu inca). 

# Material de studiu

* Un ghid scris de eugenipi: https://www.bozogrup.ro/academie/ro/regulimanagement.html
* Un studiu extrem de bine realizat si important pe Global: [17350846.1](https://www.hattrick.org/goto.ashx?path=/Forum/Read.aspx?n=1%26nm=220%26t=17350846&v=0)
