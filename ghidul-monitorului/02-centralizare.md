# Faza 2: Centralizare

## De ce

### Pentru o comunicare eficienta

Cum se poate citi din faza 1, jucatorii sunt identificati din mai multe surse. Ceea ce inseamna ca e foarte posibil ca acelasi jucator sa fie gasit de mai multe ori, din mai multe surse sau de catre mai multi monitori. Inainte ca unul din monitori sa ia contact cu proprietarul, are nevoie sa stie daca nu cumva exista un alt monitor care se ocupa deja de jucatorul respectiv. In caz contrar, e posibil ca proprietarul sa fie contactat de mai multi monitori in vederea aceluiasi jucator. Daca ne putem in locul lui, in situatia de a primi 3-4 mesaje despre acelasi jucator de la 3-4 oameni diferiti, vom observa ca asta nu are cum sa fie o experienta placuta. In plus, evitand sa avem mai multi responsabili pentru acelasi jucator, reducem numarul de mesaje pe care trebuie sa-l trimitem, ceea ce ne ajuta si pe noi.

Este de asemenea posibil ca acelasi proprietar sa aibe nu unul, ci mai multi jucatori interesanti. Inainte ca unul din monitori sa ia contact cu proprietarul, are nevoie sa stie daca nu cumva s-a luat deja contact cu proprietarul pentru un alt jucator. In caz contrar, este posibil ca doi monitori sa vina cu sfaturi diferite (bune, dar cu scop diferit). Daca ne punem in locul lui, in situatia de a primi sfaturi diferite contradictorii de la mai multi oameni, vom observa ca asta nu are cum sa fie o experienta placuta. In plus, evitand sa dam asemenea sfaturi, reducem situatiile in care unul sau mai multi dintre jucatorii aceluiasi proprietar ajung sa nu fie antrenati optim. 

### Pentru o privire de ansamblu

Chiar daca este important sa antrenam fiecare jucator optim, vedetele nu alcatuiesc neeaparat o echipa. Este important sa exista un loc unde sa avem toate datele necesare pentru a crea o privire de ansamblu asupara unei generatii de jucatori, pentru a ne asigura ca generatia nu duce lipsa de jucatori in pozitii cheie. In caz contrar, este posibil sa ne trezim cu situatii extreme, de tipul 20 de fundasi centrali antrenati optim dar doar 6 fundasi laterali.

## Cum

Fiecare jucator pe care il avem in vedere are un `issue` in acest proiect. In acest issue avem mentionat, prinre altele:

* ID-ul jucatorului - astfel ne putem folosi de functionalitatea de search din susul paginii pentru a afla daca un jucator pe care vrem sa-l centralizam exista deja centralizat.
* ID-ul echipei - astfel ne putem folosi de functionalitatea de search din susul paginii pentru a afla daca echipa jucatorului pe care vrem sa-l centralizam are si alti jucatori deja centralizati.
    * Dupa adaugarea unui jucator este bine sa trecem ceilalti jucatori ai echipei respective in sectiunea de "linked issues" pentru a putea vedea ca suntem in situatia in care o echipa are mai multi jucatori.
        * Cautam id-ul echipei (search, in dreapta sus) si retinem toti coechipierii.
        * Ne intoarcem in jucator si apasam butonul de `+` din sectiunea de linked issues.
        * Apoi scriem `#` si incepem sa scriem numele unui coechipier.
        * Va aparea o lista cu issue-urile care se potrivesc (dupa titlu).
        * O alegem pe cea corecta si apasam "Add".
        * Repetam procesul pentru fiecare coechipier identificat.
* Un label care reprezinta eligibilitatea jucatorului respectiv pentru U-21 (adica pana la ce meci poate sa joace) - astfel puteam avea o privire de ansamblu asupra a cat pot juca jucatorii pe care ii monitorizam
* Minim un label care reprezinta pozitia care i se potriveste jucatorului - astfel putem urmari ce avem de facut pe compartimente si ne putem crea o privire de ansamblu asupra lotului.
* Planul de antrenament (in descriere, pentru a-l vedea cat mai repede)
* Continutul comunicarii cu ownerul despre jucator (in comentarii, pentru a fi disponibil intregii echipe de monitorizare atunci cand monitorul jucatorului respectiv nu este, din motive personale, disponibil).
* Link-uri folositoare:
  * Link catre HT-Mail direct catre owner
  * Link catre un tracker (in cazul in care jucatorul este intr-un tracker) unde puteam vedea calitatile. In caz ca nu este, le putem trece direct in descriere.
  * Link catre pagina din HT a jucatorului

In afara de datele astea, mai sunt cateva elemente ale unui `issue` (practic ale unui jucator), de care tinem cont:

* Monitorul assignat unui jucator (dreapta sus in issue) este cel care tine legatura cu owner-ul. In felul acesta ne asiguram ca nu trimitem mai multe mesaje in acelasi timp.
* `Due date` reprezinta data la care urmatoarea comunicare trebuie realizata. (pentru indrumare sau transfer)
* Label-urile ~"To-Contact", ~"Pending-Feedback", ~"Up-To-Date" ne ajuta sa organizam jucatorii in board-uri pe departamente.
* **Confidentialitatea**. Fiecare issue poate fi `marked as confidential` (este un checkbox), ceea ce il face sa devina invizibil pentru persoanele din afara proiectului.
* Un issue poate fi oricand inchis (closed). In acest caz el va disparea din toate board-urile.

Acum urmeaza alte link-uri si mentioni folositoare:

* Lista de issue-uri: https://gitlab.com/e-random/hattrick/ro-u21-wc35/-/issues
* Lista de label-uri: https://gitlab.com/e-random/hattrick/ro-u21-wc35/-/labels
* Despre board-uri in general:
    * Board-urile se pot accesa si din meniul din stanga `Issues -> Boards`. 
    * Se poate trece de la un board la altul printr-un dropdown.
    * Un `issue` (jucator) poate fi mutat dintr-o coloana in alta cu drag & drop
    * Un `issue` (jucator) poate fi mutat si modificand label-urile din el
    * Continutul fiecarui board poate fi filtrat folosind bara de deasupra board-ului (langa dropdown-ul in care e mentionat numele board-ului)
    * **Nu folositi butonul de adaugare issue (`+`) din coloanele unui board** In loc de asta, folositi butonul de adaugare issue (`+`) din meniul din susul paginii. In acest fel, beneficiati de template-urile create pentru continut.
* Board-uri cu jucatorii
    * [Goalkeepers WC35](https://gitlab.com/e-random/hattrick/ro-u21-wc35/-/boards/2337797)
    * [Defenders WC35](https://gitlab.com/e-random/hattrick/ro-u21-wc35/-/boards/2337806)
    * [Midfielders WC35](https://gitlab.com/e-random/hattrick/ro-u21-wc35/-/boards/2337813)
    * [Wingers WC35](https://gitlab.com/e-random/hattrick/ro-u21-wc35/-/boards/2337808)
    * [Forwards WC35](https://gitlab.com/e-random/hattrick/ro-u21-wc35/-/boards/2337814)
