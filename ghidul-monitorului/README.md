# Ghidul monitorilui

## De ce

### De ce monitorizam

* Pentru a ne asigura ca echipa nationala are la dispozitie cei mai buni jucatori.
* Pentru ca ne place sa antrenam cei mai buni jucatori, dar nu tot timpul ni-i permitem.
* Pentru ca vrem sa invatam cum se antreneaza cei mai buni jucatori.
* Dar mai ales pentru ca ne face placere.

## Cum

In primul rand, monitorii sunt introdusi in proiect: 

[Ghid-Intrare-In-Proiect](./00-intrare-in-proiect.md)

Fiecare jucator pe care il monitorizam trece prin urmatoarele faze:

1. [Identificare](./01-identificare.md)
2. [Centralizare](./02-centralizare.md)
3. [Recrutare](./03-recrutare.md)
4. [Indrumare](./04-indrumare.md)
5. [Transfer](./05-transfer)

