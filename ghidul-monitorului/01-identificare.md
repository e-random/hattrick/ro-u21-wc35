# Identificare

## Ce cautam

### Varsta

Pare pueril, dar nu vrem sa ne pierdem timpul cu jucatori care nu pot fi folositi de U21 decat in cazul in care sunt exceptionali si pot ajunge mai tarziu la NT. [Aici](https://hattrickportal.pro/Tracker/U20/U21WC.aspx?WorldCup=35) putem gasi in fiecare zi varstele necesare pentru fiecare etapa a calificarilor.

**Pe noi ne intereseaza jucatorii care prind minim 5 meciuri din Continental Championship**

### Skill-uri

Skill-urile si specialitatile pe care le cautam, pentru fiecare pozitie, sunt:

*TODO: De vorbit cu coordonatorii pentru a stabili continutul acestui tabel*

## Unde cautam

### Tracker-ul HT-RO

https://www.ht-ro.org/

Primul deoarece este cel mai important. Multa munca a fost depusa de-a lungul timpului pentru a aduce cat mai multi manageri in el, aici pot fi gasiti atat jucatori seniori cat si cei din echipele de tineret.

In primul rand, coordonatorul proiectului ar trebui sa va poata da drepturi de folosire tracker.

Tracker-ul este foarte folositor deoarece odata adaugat un jucator, aveti vizibilitate asupra a aproape tot ce tine de un jucator ( cum este antrenat, cand creste un skill, etc). In plus, are deja o baza de date semnificativa de jucatori de tineret.

Dezavantajul lui este ca nu calculeaza sub-skill-ul jucatorului, multi proprietari straini ezita sa isi creeze cont si nu ofera functionalitati de centralizare a informatiilor.

#### Liste de jucatori pe tracker

Inainte de a incepe orice cautare, este foarte important sa ne cream o lista de monitorizare:

* Liste seniori: `Tracker - Scouting Lists - Rename Scouting Lists - Save`. 
* Liste juniori: `U17 - U17 Scouting Lists - Rename Scouting Lists - Save`.

Aici va denumiti lista pe care doriti sa o creati. Sfatul meu este sa ii dati un nume cat mai sugestiv, pentru a fi cat mai usor de inteles si de ceilalti utilizatori ( ex: Atacanti - WC35 - Regionale ). Mai tarziu vom centraliza jucatorii din aceasta lista, dar asta va necesita alt-tab-uri si nu vrem sa ne oprim din cautare pentru centralizare. 

#### Cautare pe tracker

* Cautare seniori: `Tracker - Player Search - Search`
* Cautare juniori: `U17 - Player Search - Search`

Aici aveti posibilitatea sa cautati jucatorii care au fost deja adaugati in tracker de ownerii lor. Setati filtrele dorite de voi ( varsta, skill, etc ) si in momentul in care gasiti un jucator interesant, pe pagina acestuia aveti optiunea sa il adaugati in lista creata anterior. (in josul paginii aveti "my scouting list - add")


#### Cautare pe list de transferuri

Vă duceți pe lista de transferuri, vă setați un profil în care să fie trecută calitatea principală minim acceptabil (dacă sunteți monitori de fundași, aceasta va fi apărare), vârsta între 17 și 17 ani, născut în România, număr maxim de zile 0-14, orice specialitate. De ce acceptabil? Puteți găsi jucători cu "secundare" specifice fundașilor cel puțin bun și poate chiar și specialitate mai bună pentru un fundaș.

In momentul in care ati gasit un jucator interesant pe lista de transferuri, va duceti pe (https://www.ht-ro.org/) si aveti posibilitatea sa il importati ( tracker - add new player ), dupa care sa vi-l adaugati in lista creata mai sus.

Recomand sa faceti cautari pe Transfer list o data la trei zile, sau (daca aveti suporter), sa setati "prospecte".

#### Cautare in Top Players

Sectiunea aceasta este destul de importanta si recomand sa fie verificata o data la 2-3 saptamani. Este foarte utila in situatia in care avem un jucator promovat de la echipa de tineret care urmeaza sa fie antrenat in echipa mama. Neajungand niciodata pe TL si daca ownerul nu il importa pe ht-ro, nu ai cum sa afli de el decat daca intri in Top Players.

Sectiunea e foarte importanta si pentru U17, pentru situatiile in care antrenorul nu este inscris pe tracker-ul nostru sau pe HT-YC.

Pentru cei care nu stiu unde se afla aceasta optiune, va rog sa urmati urmatorii pasi in hattrick: Click pe *International* (sau *World*) - iar in meniul din stanga, jos de tot, aveti rubrica *Jucatori de top* (sau *Top players*) ( depinde de limba folosita ). Sau dati click aici: [link-top-players](https://www.hattrick.org/goto.ashx?path=/World/Players/TopPlayers.aspx). 

Alegeti nationalitatea, varsta si pozitia. Taburile de sus dau posibilitatea disctinctiei intre U17 si seniori. Userii cu supporter au optiunea sa aleaga si numarul de zile dorit.

La fel si aici, in momentul in care ai gasit un jucator interesant, adauga-l pe (https://www.ht-ro.org/) Cel mai bun atacant din generatia de atacanti wc32, a fost gasit aici.

#### Cautare Tracker-ul international de seniori

https://hattrickportal.pro/Tracker

Doar antrenorii de U-21 sau de NT pot da drepturi aici.

Toate instructiunile de utilizare si de cautare, le gasiti aici: https://hattrickportal.pro/Help

Acest tracker este foarte util in momentul in care avem jucatori antrenati de owneri straini. Noi suntem printre putinele tari care au propriul tracker 90% folosindu-l pe acesta. In plus, acest tracker calculeaza si subskill-ul jucatorilor urmariti.

Dezavantajul acestui tracker este ca ii lipsesc cateva din statisticile pe jucator oferite de tracker-ul nostru. Din acest motiv, in momentul in care se ia contact cu proprietarii de jucatori, ei ar trebui indreptati catre ambele trackere, nu doar al nostru respectiv nu doar cel international.

### Cautare in Hattrick Youthclub

https://www.hattrick-youthclub.org/

Aceasta aplicatie pentru management-ul echipei de tineret are si sectiune de tracking. Este disponibila in meniul `My HT -> NT Scouting`. Doar antrenorii de U-21 sau de NT pot da drepturi aici.

Avantajul acestui tracker este ca ofera mai multe informatii despre jucator decat trakcer-ul nostru. Specialitati deduse din meciuri, subskill, sunt cateva exemple.

Dezavantajul este ca are relativ putini manageri romani si este ceva mai complicat de folosit.

Din aceste motive, antrenorii cu care comunicam pentru tineret ar trebui indreptati spre ambele trackere, atat al nostru cat si acesta. Functionalitatile acestuia i-ar putea ajuta sa invete sa-si creasca mai bine jucatorii si le-ar putea oferi si lor si noua mai multe informatii despre ei, in timp ce al nostru e mai usor de folosit pentru cautari.

#### Cautare pe conferinte

Putem tine un ochi pe topicurile dedicate postarii jucatorilor pe conferinta nationala si conferinta de incepatori. Aici nu pot da link-uri deoarece ID-urile lor se schimba constant.
