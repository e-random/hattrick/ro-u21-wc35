# Recrutare

In aceasta faza este important sa stabilim un contact cu proprietarii jucatorilor, sa aflam ce planuri au.

Acesta este template-ul pe care eu l-as folosi pentru primul mesaj:

```
Salut!

Sunt în echipa de monitorizare pentru U21, generația pentru WC 35. Noi ne organizam proiectul aici: 

[link=https://gitlab.com/e-random/hattrick/ro-u21-wc35]

Îți scriu să te anunț că jucatorul tau prezintă interes. Notele noastre despre el sunt aici:

[link=https://gitlab.com/e-random/hattrick/ro-u21-wc35/-/issues/XXX].

Te rog să mă anunți ce planuri ai cu el și să mă contactezi cu orice întrebare.

Spor la antrenat!

PS. Noi am scris un ghid pe scurt despre management-ul optim al academiei. E posibil sa gasesti in el sfaturi folositoare. Este disponibil aici: 

[link=https://gitlab.com/e-random/hattrick/ro-u21-wc35/-/tree/master/ghidul-academiei]
```

Cateva note despre acest mesaj:

* E important ca antrenorii sa stie cum pot verifica faptul ca eu chiar fac parte din echipa de monitorizare, si din acest motiv dau primul link, unde se vede intreaga echipa de monitorizare.
* E important ca antrenorii sa stie ce note avem despre jucatorul lor astfel incat sa ne poata corecta in cazul in care avem ceva gresit. Din acest motiv dau al doilea link.
* De asemenea e important sa stie ca acestea sunt publice, pentru a putea cere marcarea lor ca si confidentiale in cazul in care nu sunt confortabili cu asta. Si din acest motiv dau al doilea link.
* Pentru al doilea link, el va diferi de la jucator la jucator in zona cu XXX, unde este id-ul `issue-ului` pe care il avem noi deschis pentru el. E singurul loc din mesaj de care trebuie sa ma ating pentru fiecare owner.
* Pun numele si ID-ul jucatorului in titlul mesajului. In acest fel nu trebuie sa mai dau un copy paste in interiorul mesajului cu informatia asta.
* Mesajul se poate folosi in aceasta forma chiar daca antrenorul are mai mult de un jucator in vederea noastra sau a altor proiecte, vom fi interesati de acest aspect abia in faza de indrumare.
