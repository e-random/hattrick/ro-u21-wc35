# Intrare in proiect

## Inainte de a intra

Asigurati-va ca aveti conturi pe site-urile pe care cautam jucatori:

* https://ht-ro.org
* https://hattrick-youthclub.org
* https://hattrickportal.pro/Tracker

Asigurati-va ca aveti conturi pe platformele pe care le folosim pentru comunicare:

* Discord: https://discord.gg/sYapbuRyR2 - pentru discutii mai putin formale
* GitLab: In cazul in care nu aveti deja cont pe GitLab el poate fi creat aici: https://gitlab.com/users/sign_up

Cereti access la proiectul din GitLab:

E vorba chiar de proiectul din care cititi acum. Link direct la pagina principala a proiectului: https://gitlab.com/e-random/hattrick/ro-u21-wc35. In caz ca nu observati imediat link-ul pentru "Request Access" pe pagina proiectului (atentie, trebuie sa fiti logati ca sa apara), exista aici un printscreen care arata unde ar trebui sa fie: https://docs.gitlab.com/ce/user/project/members/#project-membership-and-requesting-access 


## Imediat dupa ce ati primit access.

Creati un issue in care cereti:

1. Sa fiti adaugati pe lista de membri pe pagina proiectului.
2. Sa primiti access pe platformele de cautare jucatori (mentionati daca aveti deja).

## Despre issue-uri

* De cate ori participati intr-un issue (cu comentarii sau daca il deschideti), va inscrieti pentru a primi notificari pe update-urile care apar in inssue-ul respectiv.
* Daca cineva va va mentiona in mod explicit, cu, de exemplu, @KingRandom, asta se va adauga in lista voastra personala de "To Do" pe care o gasiti aici: https://gitlab.com/dashboard/todos. Incercati sa raspundeti ori de cate ori e necesar si sa tineti lista respectiva goala.
    * Lista de "To Do" este disponibila din partea de dreapta sus, langa profil. Daca n-o vedeti din prima, printscreen-urile de aici va pot indrepta spre ea (retineti ca puteti adauga si to-do manual, detalii in acelasi link) https://docs.gitlab.com/ce/user/todos.html
* Issue-urile sunt de doua tipuri:
    * Normale. Pur si simplu task-uri care trebuiesc rezolvate.
    * Jucatori. Tin informatii despre jucatorii pe care ii avem sub monitorizare.

## Despre jucatori si board-uri

Folsim cate un issue pentru fiecare jucator pentru a ne fi usor sa ii organizam vizual, in board-uri. Exista un template care precompleteaza o parte din informatiile necesare, in el trebuie trecut id-ul jucatorului, id-ul echipei din care face parte, etc (locurile in care trebuie puse informatii sunt marcate cu `NNN` sau `XXX`, acceptam propuneri de imbunatatire la structura). Template-ul poate fi aplicat atat cand apasati "New Issue" cat si cand apasati "Edit" la un issue existent.

Board-urile sunt disponibile din meniul din stanga, "Issues -> Boards". Link direct: https://gitlab.com/e-random/hattrick/ro-u21-wc35/-/boards

_Nota: Board-urile nu se observa foarte bine pe ecrane mici. Utilizatorii de mobil sunt rugati sa foloseasca optiunea "Request Desktop Site" pe pagina cu board-uri. Unul din print-screen-urile de aici ar trebui sa ajute (difera de la device la device si de la browser la browser): [Google Image Search: request desktop version](https://www.google.com/search?tbm=isch&q=request+desktop+version). De remarcat ca meniul navigational din stanga nu functioneaza cu optiunea asta (deci s-ar putea sa trebuiasca sa o bifati/debifati in functie de cum aveti nevoie de el)._

Label-urile sunt niste etichete aplicate issue-urilor. Ele sunt folosite de catre board-uri pentru a trece jucatorii in coloana potrivita si pentru a alege continutul fiecarui board. De acea este important sa avem label-uri corecte pe jucatori, pentru o vizualizare corecta.

Note:

* Un label se poate aplica sau scoate din meniul din dreapta.
* Mai exista si comenzi scurte scrise in mesage. `/label` respectiv `/unlabel` sunt comenzile scurte pentru a adauga sau scoate label-uri, dar o lista completa se gaseste in link-ul de "quick actions" care va apare sub message box de fiecare data cand scrieti un comment.
* In momentul in care un issue este tras dintr-o lista in alta intr-un board cele doua label-uri implicate sunt automat sincronizate.
* Pentru utilizatorii de mobile, in versiunea de "desktop version", tragerea se poate efectua cu "long tap" (tii apasat pe issue si incepi sa tragi de el)

De cate ori adaugati un comment aveti grija sa mentionati in mod specific persoana de la care doriti sa primiti raspuns, sau care doriti sa observe ca s-a facut un update. 

## Organizare continut

Proiectul actual ar trebui sa contina tot ce aveti nevoie pentru a desfasura activitatea. Daca lipseste ceva, creati un issue pentru a fi adaugat.

Puteti cauta oricand, orice in interiorul proiectului folosind search-box-ul din dreapta sus. De exemplu:
* Cand adaugati un jucator, cautati id-ul lui pentru a vedea daca nu-l are deja altcineva in vedere.
* Cand adaugati un jucator, cautati id-ul echipei din care face parte pentru a vedea daca acea echipa are si alti jucatori in vederile noastre. Folositi cu incredere butonul de + de la sectiunea de "Linked issues" dupa ce l-ati adaugat pentru a crea legaturi intre jucatorii aceleiasi echipe.
* Daca aveti nevoie de ghidul academiei, sau ghidul gitlab, sau orice altceva, doar scrieti cuvintele cheie in search si veti primi link direct la ele.
* Cand incetati sa monitorizati un jucator, sau rezolvati un task, pur si simplu inchideti issue-ul aferent. Va disparea din board-uri (poate fi in continuare gasit cu search sau vazut in lista completa de issue-uri).
* Aveti mare grija sa marcati issue-uri "confidentiale" daca ele contin informatii sensibile sau ownerii cer asta!
* Orice activitate din interiorul proiectului este raportata pe un canal de discord. La acest canal au access doar membrii proiectului din gitlab.

## Backup and restore

Toate conversatiile care au loc in GitLab intra intr-un backup. Pe de alta parte, cele de pe Discord _nu_ intra intr-un backup. Deci orice conversatii importante si note pe jucatori care trebuie sa ramana trebuie adaugate in GitLab.